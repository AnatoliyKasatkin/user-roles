# User Roles Editor with Data Table.

> Features: [Angular 6](https://angular.io/), [Bootstrap 4](https://getbootstrap.com/), [ng-bootstrap](https://ng-bootstrap.github.io/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.1.

## Basic usage.

Run `$ ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `$ ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

Run `$ ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `$ ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

Run `$ yarn docs` - to generate a static documentation via [Compodoc](https://compodoc.github.io/website/). Navigate to `http://localhost:9000/`.

## Overview.

![App Structure](src/assets/user-roles.jpg)
