import { Routes } from '@angular/router';

export const appRoutes: Routes = [{
    loadChildren: './home/home.module#HomeModule',
    path: '',
    pathMatch: 'full'
  }, {
    loadChildren: './users/users.module#UsersModule',
    path: 'users'
  }, {
    path: '**',
    redirectTo: ''
}];
