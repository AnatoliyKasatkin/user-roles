import { Component, Input, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../../core/user/user.service';

@Component({
  selector: 'app-roles-modal',
  styleUrls: ['./roles-modal.component.scss'],
  templateUrl: './roles-modal.component.html'
})
export class RolesModalComponent implements OnInit {
  @Input() person;

  disabled = true;
  initPerson: any;
  rights: any;
  user: any;

  constructor(
    public activeModal: NgbActiveModal,
    private userService: UserService,
  ) {
    this.rights = this.userService.getUserRights();
  }

  ngOnInit(): void {
    this.initPerson = Object.assign({}, this.person);
    this.user = Object.assign({}, this.person);
    this.updateRights();
  }

  editRights(right: any): void {
    this.user.includes = right.includes;
    this.user.rights = right.id;
    this.user.rightsDesc = right.desc;
    this.updateRights();
    this.disabled = this.user.rights === this.initPerson.rights;
  }

  saveRights(): void {
    if (this.disabled) return;
    this.person = this.user;
    this.userService.setUserRights(this.person);
  }

  resetRights(): void {
    if (this.disabled) return;
    this.disabled = true;
    this.user = Object.assign({}, this.initPerson);
    this.updateRights();
  }

  private updateRights(): void {
    this.rights.map(right => {
      right.active = !!this.user.includes.filter(id => id === right.id).length;
      right.primary = right.id === this.user.rights;
    });
  }
}
