import { RouterModule } from '@angular/router';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

import { EnumLikeToArrayPipe } from '../core/pipes/enum-like-to-array.pipe';
import { LayoutService } from '../core/layout/layout.service';
import { RolesModalComponent } from './roles-modal/roles-modal.component';
import { RolesTableComponent } from './roles-table/roles-table.component';
import { TableModule } from '../table/table.module';
import { UsersComponent } from './users.component';
import { UserService } from '../core/user/user.service';

@NgModule({
  declarations: [
    RolesModalComponent,
    RolesTableComponent,
    UsersComponent
  ],
  entryComponents: [
    RolesModalComponent,
    RolesTableComponent
  ],
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    NgbPaginationModule,
    RouterModule.forChild([{ component: UsersComponent, path: '' }]),
    TableModule
  ],
  providers: [
    EnumLikeToArrayPipe,
    LayoutService,
    TitleCasePipe,
    UserService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UsersModule {}
