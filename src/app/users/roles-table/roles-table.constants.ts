export const rolesTableColumns: Array<any> = [{
    filtering: { filterString: '', placeholder: 'Filter by name' },
    name: 'name',
    title: 'Full Name'
  }, {
    filtering: { filterString: '', placeholder: 'Filter by position' },
    name: 'position',
    title: 'Position'
  }, {
    filtering: { filterString: '', placeholder: 'Filter by date' },
    name: 'lastVisit',
    title: 'Last Activity'
  }, {
    filtering: { filterString: '', placeholder: 'Filter by rights' },
    name: 'rightsDesc',
    sort: 'asc',
    title: 'Rights'
}];

export const rolesTableConfig = {
  actions: true,
  paging: true,
  sorting: { columns: rolesTableColumns },
  filtering: { filterString: '' },
  className: ['table-bordered', 'table-striped']
};
