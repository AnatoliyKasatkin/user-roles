import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';

import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';

import { Subscription } from 'rxjs';

import { LayoutService } from '../../core/layout/layout.service';
import { RolesModalComponent } from '../roles-modal/roles-modal.component';
import { UserService } from '../../core/user/user.service';
import { rolesTableColumns, rolesTableConfig } from './roles-table.constants';

@Component({
  encapsulation: ViewEncapsulation.None,
  providers: [NgbPaginationConfig],
  selector: 'app-roles-table',
  styleUrls: ['./roles-table.component.scss'],
  templateUrl: './roles-table.component.html'
})
export class RolesTableComponent implements OnDestroy {
  collectionSize = 0;
  columns: Array<any> = rolesTableColumns;
  config = rolesTableConfig;
  itemsPerPage = 8;
  maxSize = 5;
  mediaLayout: string;
  numPages = 1;
  page = 1;
  rows: Array<any> = [];

  private data$: Subscription;
  private data: Array<any> = [];
  private layout$: Subscription;

  constructor(
    private layoutService: LayoutService,
    private modalService: NgbModal,
    private userService: UserService
  ) {
    this.layout$ = this.layoutService.breakpoint$.subscribe(breakpoint => this.mediaLayout = breakpoint.mediaLayout);
    this.data$ = this.userService.getRolesTableData().subscribe(data => {
      this.data = data;
      this.collectionSize = this.data.length;
      this.numPages = Math.ceil(this.collectionSize / this.itemsPerPage);
      this.onChangeTable(this.config);
    });
  }

  changePage(page: any, data: Array<any> = this.data): Array<any> {
    const start = (page.page - 1) * page.itemsPerPage;
    const end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  changeSort(data: any, config: any): any {
    if (!config.sorting) return data;

    const columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) return data;

    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) return sort === 'desc' ? -1 : 1;
      else if (previous[columnName] < current[columnName]) return sort === 'asc' ? -1 : 1;
      return 0;
    });
  }

  changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) return filteredData;

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    const tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) flag = true;
      });
      if (flag) tempArray.push(item);
    });
    filteredData = tempArray;

    return filteredData;
  }

  onChangeTable(config: any, pageNum: number = 1): void {
    this.page = pageNum;

    const page: any = {
      page: this.page,
      itemsPerPage: this.itemsPerPage
    };

    if (config.filtering) Object.assign(this.config.filtering, config.filtering);

    if (config.sorting) Object.assign(this.config.sorting, config.sorting);

    const filteredData = this.changeFilter(this.data, this.config);
    const sortedData = this.changeSort(filteredData, this.config);

    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.collectionSize = sortedData.length;
  }

  onCellClick($event): void {}

  onActionInit($event): void {
    const modalRef = this.modalService.open(RolesModalComponent, { centered: true });
    modalRef.componentInstance.person = $event;
  }

   ngOnDestroy(): void {
      this.data$.unsubscribe();
      this.layout$.unsubscribe();
   }
}
