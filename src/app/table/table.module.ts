import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TableComponent } from './table.component';
import { TableFilteringDirective } from './table-filtering.directive';
import { TableSortingDirective } from './table-sorting.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [
    TableComponent,
    TableFilteringDirective,
    TableSortingDirective
  ],
  exports: [
    TableComponent,
    TableFilteringDirective,
    TableSortingDirective
  ]
})
export class TableModule {}
