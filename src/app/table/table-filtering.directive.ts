import { Directive, EventEmitter, ElementRef, Renderer2, HostListener, Input, Output } from '@angular/core';

function setProperty(renderer: Renderer2, elementRef: ElementRef, propName: string, propValue: any): void {
  renderer.setProperty(elementRef, propName, propValue);
}

@Directive({
  selector: '[appTableFiltering]'
})
export class TableFilteringDirective {
  @Input() appTableFiltering: any = {
    filterString: '',
    columnName: 'name'
  };

  @Output() tableChanged: EventEmitter<any> = new EventEmitter();

  @Input()
  get config(): any {
    return this.appTableFiltering;
  }

  set config(value: any) {
    this.appTableFiltering = value;
  }

  private element: ElementRef;
  private renderer: Renderer2;

  constructor(
    element: ElementRef,
    renderer: Renderer2
  ) {
    this.element = element;
    this.renderer = renderer;
    setProperty(this.renderer, this.element, 'value', this.appTableFiltering.filterString);
  }

  @HostListener('input', ['$event.target.value'])
  onChangeFilter(event: any): void {
    this.appTableFiltering.filterString = event;
    this.tableChanged.emit({ filtering: this.appTableFiltering });
  }
}
