import { Directive, EventEmitter, Input, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[appTableSorting]'
})
export class TableSortingDirective {
  @Input() appTableSorting: any;
  @Input() column: any;
  @Output() sortChanged: EventEmitter<any> = new EventEmitter();

  @Input()
  get config(): any {
    return this.appTableSorting;
  }

  set config(value: any) {
    this.appTableSorting = value;
  }

  @HostListener('click', ['$event'])
  onToggleSort(event: any): void {
    if (event) event.preventDefault();

    if (this.appTableSorting && this.column && this.column.sort !== false) {
      switch (this.column.sort) {
        case 'asc':
          this.column.sort = 'desc';
          break;
        case 'desc':
          this.column.sort = '';
          break;
        default:
          this.column.sort = 'asc';
          break;
      }
      this.sortChanged.emit(this.column);
    }
  }
}
