import { Component, ViewEncapsulation } from '@angular/core';

import { LayoutService } from './core/layout/layout.service';
import { NavbarComponent } from './core/navbar/navbar.component';
import { SidebarComponent } from './core/sidebar/sidebar.component';

@Component({
  encapsulation: ViewEncapsulation.None,
  entryComponents: [
    NavbarComponent,
    SidebarComponent
  ],
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html'
})
export class AppComponent {
  mediaLayout: string;
  sidebarActive = false;

  constructor(private layoutService: LayoutService) {
    this.layoutService.breakpoint$.subscribe(breakpoint => this.mediaLayout = breakpoint.mediaLayout);
  }

  toggleSidebar(): void {
    document.body.classList.toggle('modal-open');
    this.sidebarActive = !this.sidebarActive;
  }
}
