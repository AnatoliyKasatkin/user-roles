import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild([{ component: HomeComponent, path: '' }])
  ]
})
export class HomeModule {}
