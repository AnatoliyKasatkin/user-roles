import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-navbar',
  styleUrls: ['./navbar.component.scss'],
  templateUrl: './navbar.component.html'
})
export class NavbarComponent {
  @Input() mediaLayout: string;
  @Output() toggledSidebar = new EventEmitter<boolean>();

  toggleSidebar($event): void {
    this.toggledSidebar.emit($event);
  }
}
