import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  styleUrls: ['./sidebar.component.scss'],
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent {
  @Input() mediaLayout: string;
}
