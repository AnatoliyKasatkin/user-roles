import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToArray'
})
export class EnumLikeToArrayPipe implements PipeTransform {
  transform(data: Object): Array<any> {
    return Object.keys(data).map(key => data[key]);
  }
}
