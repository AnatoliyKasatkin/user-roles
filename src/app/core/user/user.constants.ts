export const USER_RIGHTS = {
  RIGHT_1: { id: 1, includes: [], desc: 'right 1' },
  RIGHT_2: { id: 2, includes: [1, 3], desc: 'right 2' },
  RIGHT_3: { id: 3, includes: [2, 4, 5], desc: 'right 3' },
  RIGHT_4: { id: 4, includes: [1, 5], desc: 'right 4' },
  RIGHT_5: { id: 5, includes: [], desc: 'right 5' },
  RIGHT_6: { id: 6, includes: [5], desc: 'right 6' },
  RIGHT_7: { id: 7, includes: [6], desc: 'right 7' }
};
