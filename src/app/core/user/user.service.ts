import { Injectable } from '@angular/core';

import { TitleCasePipe } from '@angular/common';
import { Observable, ReplaySubject } from 'rxjs';

import { EnumLikeToArrayPipe } from '../pipes/enum-like-to-array.pipe';
import { mockUsers } from './mock-user-data';
import { USER_RIGHTS } from './user.constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  rights: any = this.enumToArray.transform(USER_RIGHTS);
  rolesTableData$: ReplaySubject<any> = new ReplaySubject<any>();
  users: Array<any> = mockUsers;

  constructor(
    private enumToArray: EnumLikeToArrayPipe,
    private titlecasePipe: TitleCasePipe
  ) {
    if (localStorage.getItem('users')) this.users = JSON.parse(localStorage.getItem('users'));
    else localStorage.setItem('users', JSON.stringify(this.users));
    this.toRolesTableData(this.users);
  }

  getRolesTableData(): Observable<any> {
    return this.rolesTableData$.asObservable();
  }

  getUserRights(): Array<any> {
    return this.rights;
  }

  setUserRights(user: any): void {
    this.users.map(person => {
      if (person.id === user.id) person.rights = Array.of(user.rights);
    });
    localStorage.setItem('users', JSON.stringify(this.users));
    this.toRolesTableData(this.users);
  }

  private toRolesTableData(arr: Array<any>): void {
    const res = arr.map(person => {
      const resDate: any = {};
      let date: string | Date = '-';
      try {
        date = new Date(person.lastVisit);
        resDate.date = ('0' + date.getUTCDate()).slice(-2);
        resDate.month = ('0' + date.getUTCMonth()).slice(-2);
        resDate.year = date.getUTCFullYear();
        resDate.hours = ('0' + date.getUTCHours()).slice(-2);
        resDate.minutes = ('0' + date.getUTCMinutes()).slice(-2);
        date = `${ resDate.date }.${ resDate.month }.${ resDate.year } ${ resDate.hours }:${ resDate.minutes }`;
      } catch (e) {
        throw new Error(`Date of the last visit is not a valid!\nGotta be in ISO 8601 Extended format.\nUser ID: ${ person.id }`);
      }
      return ({
        id: person.id,
        includes: USER_RIGHTS['RIGHT_' + person.rights[0]].includes,
        lastVisit: date,
        name: this.titlecasePipe.transform(`${ person.surname } ${ person.name } ${ person.patronymic }`),
        position: person.position.charAt(0).toUpperCase() + person.position.slice(1),
        rights: person.rights[0],
        rightsDesc: this.titlecasePipe.transform(USER_RIGHTS['RIGHT_' + person.rights[0]].desc)
      });
    });
    this.rolesTableData$.next((res || []));
  }
}
