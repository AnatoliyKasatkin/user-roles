export interface AppBreakpoint {
  mediaLayout: string | undefined;
  mediaQuery: string | undefined;
}

// Bootstrap 4 responsive breakpoints.
export const appBreakpoints: AppBreakpoint[] = [{
    mediaLayout: 'xl',
    mediaQuery: '(min-width: 1200px)'
  }, {
    mediaLayout: 'lg',
    mediaQuery: '(min-width: 992px) and (max-width: 1199px)'
  }, {
    mediaLayout: 'md',
    mediaQuery: '(min-width: 768px) and (max-width: 991px)'
  }, {
    mediaLayout: 'sm',
    mediaQuery: '(min-width: 576px) and (max-width: 767px)'
  }, {
    mediaLayout: 'xs',
    mediaQuery: '(min-width: 320px) and (max-width: 575px)'
}];
