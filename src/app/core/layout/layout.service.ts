import { Injectable } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';

import { ReplaySubject } from 'rxjs/index';
import { distinctUntilChanged } from 'rxjs/internal/operators';

import { appBreakpoints, AppBreakpoint } from './layout.constants';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  breakpoint$: ReplaySubject<AppBreakpoint> = new ReplaySubject<AppBreakpoint>();

  private breakpoints: AppBreakpoint[] = appBreakpoints;

  constructor(private breakpointObserver: BreakpointObserver) {
    this.breakpoints.forEach(breakpoint => {
      this.breakpointObserver
        .observe(breakpoint.mediaQuery)
        .pipe(distinctUntilChanged())
        .subscribe(mediaQuery => {
          if (mediaQuery.matches) this.breakpoint$.next(breakpoint);
        });
    });
  }
}
